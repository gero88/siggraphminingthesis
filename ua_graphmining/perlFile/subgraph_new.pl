use strict;
use warnings;
use List::Util qw(first max maxstr min minstr reduce shuffle sum);

#Run example data set
#perl subgraph.pl -graph example/example_graph.txt -vertices example/example_vertexset.txt -labels example/example_labels.txt -debug -maxsize 2 -uniquelabel


#Default settings
our $debug = 0;
our $maxsize = 5;
our $pvalue = 0.05;
my $supportcutoff = 0;

my$graphfile;
my$labelfile;
my$groupfile;
my$outputfile;
my$bgfile;
my$singlelabel;
my$nestedpval;
my$undirected = 0;

if($#ARGV < 0){
	help_and_exit();
}

foreach my$i (0 .. $#ARGV){
	if(($ARGV[$i] eq '-graph') and ($i < $#ARGV)){
		$i++;
		$graphfile = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-vertices') and ($i < $#ARGV)){
		$i++;
		$groupfile = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-labels') and ($i < $#ARGV)){
		$i++;
		$labelfile = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-label') and ($i < $#ARGV)){
		$i++;
		$labelfile = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-bgfile') and ($i < $#ARGV)){
		$i++;
		$bgfile = $ARGV[$i];
	}elsif (($ARGV[$i] eq '-maxsize') and ($i < $#ARGV)){
		$i++;
		$maxsize = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-output') and ($i < $#ARGV)){
		$i++;
		$outputfile = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-pvalue') and ($i < $#ARGV)){
		$i++;
		$pvalue = $ARGV[$i];
	} elsif (($ARGV[$i] eq '-support') and ($i < $#ARGV)){
		$i++;
		$supportcutoff = $ARGV[$i];
	} elsif ($ARGV[$i] eq '-debug'){
		$debug = 1;
	} elsif ($ARGV[$i] eq '-uniquelabel'){
		$singlelabel = 1;
	} elsif ($ARGV[$i] eq '-nestedpval'){
		$nestedpval= 1;
	} elsif ($ARGV[$i] eq '-undirected'){
		$undirected= 1;
	}
}


#Check if all necessary variables a defined
if(not(defined($graphfile))){
	die "Please specify graph file with -graph\n";
}
if(not(defined($groupfile))){
	if($supportcutoff == 0){
		die "Please specify vertices file of interest with -vertices\n";
	}
}

our%labelhash;

#Read network
our(%graph,%label,%reversegraph);
open(GRAPH,$graphfile) or die "Cannot open ".$graphfile."\n";
while(<GRAPH>){
	chomp;
	my@l = split("\t");
	
	my$source = $l[0];
	my$target = $l[1];
	
	$reversegraph{$target}{$source} = 1;
	$graph{$source}{$target} = 1;
	
	if(not($singlelabel)){
		$label{$source}{'~'} = 1;
		$label{$target}{'~'} = 1;
		$labelhash{'~'}++;
	}
}
close GRAPH;

if(scalar keys %graph < 2){
	die "Could not find graph in file, please check formatting\n";
}

#Read labels (if available)

if(defined($labelfile)){
	open(LABEL,$labelfile) or die "Cannot open ".$labelfile."\n";
	while(<LABEL>){
		chomp;
		my@l = split("\t");
		if($#l > 0){
			$label{$l[0]}{$l[1]} = 1;
			$labelhash{$l[1]}++;
		}
	}
	close LABEL;
} else {
	if($singlelabel){
		die "Please provide a label file where each node has exactly one label in 'single-label-mode'\n";
	}
}

#Get a list of all unique labels
our@possiblelabels = sort {$labelhash{$b} <=> $labelhash{$a}} keys %labelhash;

#Read vertices of interest
my%group;
my%bgnodes;
if(defined($groupfile)){
	open(GROUP,$groupfile) or die "Cannot open ".$groupfile."\n";
	while(<GROUP>){
		chomp;
		s/\r//i;
		my$node = $_;
		if(exists($label{$node})){
			$group{$node} = 1;
		} else {
			print $node." not in graph, excluded from interesting vertices\n";
		}

	}
	close GROUP;
	
	if(scalar(keys(%group)) < 2){
		die "No interesting vertices found in file, please check formatting\n";
	}
	
	if(defined($bgfile)){
		open(BG,$bgfile) or die "Cannot open ".$bgfile."\n";
		while(<BG>){
			chomp;
			s/\r//i;
			my$node = $_;
			if(exists($label{$node})){
				$bgnodes{$node} = 1;
			} else {
				print $node." not in graph, excluded from background\n";
			}
		}
		close BG;
		if(scalar(keys(%bgnodes)) < 2){
			die "No background vertices found in file, please check formatting\n";
		}
	} else {
		%bgnodes = %label;
	}
} else { #If no group file is given, but a support is, run in normal frequent subgraph mining mode
	%group = %label;
	%bgnodes = %label;
}

if($debug){
	print "Found ".keys(%label)." nodes in graph\n";
	print "Of which ".keys(%bgnodes)." are part of the background\n";
	print "Of which ".keys(%group)." were selected\n";
	print "Of which ".keys(%graph)." have targets\n";
	print "Of which ".keys(%reversegraph)." are targets\n";
	print "With ".keys(%labelhash)." possible labels\n";
}

#This will contain all subgraphs for which the significance has been calculated
our %checkedmotifs;
our %sigmotifs;
our %freqmotifs;

#This will contain the transformations from non-canonical to canonical representation
our %motiftransformations;

#Support threshold calculation
our$totalvertices =scalar keys %bgnodes;
our$interestingvertices = scalar keys %group;
if($supportcutoff == 0){
	#Estimate number of subgraphs to be tested
	my$estimate = (2**(int(($maxsize**2)/2)))*(scalar(@possiblelabels)**$maxsize);
	my$corrpval = $pvalue/$estimate;

	#Estimate corresponding support
	for my$i (1 .. $interestingvertices){
		my$prob = upper_hypercdf($interestingvertices,$totalvertices-$interestingvertices,$i,$i);
		if($prob < $corrpval){
			$supportcutoff = $i;
			last;
		}
	}

	if($supportcutoff > 0){
		print "Subgraph support set at ".$supportcutoff." due to upper bound Pvalue\n";
	} else {
		$supportcutoff = $interestingvertices;
		print "Subgraph support set at ".$supportcutoff." for number of interesting vertices\n";
	}
}

#Iterate over all groups

foreach my$sourcelabel (@possiblelabels){
	
	unless($undirected){
		#Self edge test
		my@selfmotif = ("1".$sourcelabel."-1".$sourcelabel);
	

		build_motif(\@selfmotif,$supportcutoff,\%group,\%bgnodes);
		
	}

	foreach my$targetlabel (@possiblelabels){
	
		if($undirected == 1){
		
			#Single edge to start building from
			my@forwardmotif = ("1".$sourcelabel."-2".$targetlabel);

			build_motif_und(\@forwardmotif,$supportcutoff,\%group,\%bgnodes);
		
		} else {
			#Single edge to start building from
			my@forwardmotif = ("1".$sourcelabel."-2".$targetlabel);

			build_motif(\@forwardmotif,$supportcutoff,\%group,\%bgnodes);
		
		
		
			#Single edge to start building from
			my@backwardmotif = ("2".$sourcelabel."-1".$targetlabel);

			build_motif(\@backwardmotif,$supportcutoff,\%group,\%bgnodes);
		
		}
	}
}

if(defined($groupfile)){
	#Recalculate bonferonni correction to actual cutoff
	my$bonferonni = $pvalue / (scalar keys %sigmotifs);
	
	if($debug){print "\nChecked ".(scalar keys %sigmotifs)." subgraph(s)\n"}
	if($debug){print "Bonferonni-corrected P-value cutoff =  ".$bonferonni."\n"}

	#Output significant motifs
	if(defined($outputfile)){
		open(OUT,'>'.$outputfile);
		print OUT join("\t",("Motif","FreqS","FreqT","Pvalue"))."\n";
	} else {
		print "\n".join("\t",("Motif","FreqS","FreqT","Pvalue"))."\n";
	}
	foreach my$motif (keys %sigmotifs){
		if($sigmotifs{$motif} <= $bonferonni){
			my$motifstr = $motif;
			$motifstr =~ s/(\d)~([-,])/$1$2/g;
			$motifstr =~ s/(\d)~$/$1/g;
			if(defined($outputfile)){
				print OUT join("\t",($motifstr,$checkedmotifs{$motif},$freqmotifs{$motif},$sigmotifs{$motif})),"\n";
			} else {
				print join("\t",($motifstr,$checkedmotifs{$motif},$freqmotifs{$motif},$sigmotifs{$motif})),"\n";
			} 
		}
	}

	if(defined($outputfile)){
		close OUT;
	}
} else {
	foreach my$motif (keys %freqmotifs){
		print join("\t",($motif,$freqmotifs{$motif})),"\n";
	}
}


###############
# Subroutines #
###############

#--------------------- Directed Graph --------------------------

sub build_motif {
	
	my$motifref = shift @_;
	my$threshold = shift @_;
	my$group = shift @_;
	my$prevhits = shift @_;
	
	my@motif = @{$motifref};
	
	my%motiflabels;
	my$motifstring = join(",",@motif);
	my%nodetargets;
	my%nodesources;
	
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+)(.+)-(\d+)(.+)$/i){
			$motiflabels{$1} = $2;
			$motiflabels{$3} = $4;
			$nodetargets{$3}++;
			$nodesources{$1}++;
		}
	}
	my$maxid = scalar keys %motiflabels;
	
	if($debug){print "Starting on ".$motifstring." \n";}

				
	my$hitsupport = 0; #Support with interesting nodes
	my%hitnodes;
	
	my$checkedgroupnodes = 0;
	
	foreach my$node (keys %{$group}){ #Check each interesting node if they are a root node
		
		
		#Check if part of the prevhit set
		unless(exists(${$prevhits}{$node})){next;}
		
		$checkedgroupnodes++;
		
		#Check if first node matches label (otherwise skip)
		unless(exists($label{$node}{$motiflabels{1}})){next;}
		
		my%supmatch = (1 => $node);
		my$supmatchref = match_subgraph(\@motif,\%supmatch);
		if($supmatchref){
				$hitsupport++;
				$hitnodes{$node} = 1;
		} 
	}
	
	$checkedmotifs{$motifstring} = $hitsupport;
	
	my$biggerfim = 0;
	
	unless($hitsupport >= $threshold){
		
		#Stop with this subgraph if it did not pass the required support
		return $biggerfim; 
		
	}
	
	$biggerfim++;
		
	my$totalsupport = 0; #Support with all nodes
	foreach my$node (keys %{$prevhits}){ #Go over all nodes
		
		#Check if already run with group nodes
		if(exists(${$group}{$node})){
			if(exists($hitnodes{$node})){
				$totalsupport++;
			}
			next;
		}
		
		#Check if first node matches label (otherwise skip) and has the required edges
		unless(exists($label{$node}{$motiflabels{1}})){next;}
		if(exists($nodesources{1})){
			if(exists($graph{$node})){
				if(scalar(keys(%{$graph{$node}})) < $nodesources{1}){next;}
			} else {next;}
		}
		if(exists($nodetargets{1})){
			if(exists($reversegraph{$node})){
				if(scalar(keys(%{$reversegraph{$node}})) < $nodetargets{1}){next;}
			} else {next;}
		}
		
		my%supmatch = (1 => $node);
		my$supmatchref = match_subgraph(\@motif,\%supmatch);
		if($supmatchref){
				$totalsupport++;
				$hitnodes{$node} = 1;
		}
	}
	
	#Calculate significance
	my$prob;
	if($nestedpval){
		#Nested P-value behaviour: Enrichment P-value is calculated against parent subgraph matches
		$prob = upper_hypercdf($checkedgroupnodes,(scalar keys %{$prevhits})-$checkedgroupnodes,$totalsupport,$hitsupport);
	} else {
		#Normal behaviour: Enrichment p-value is calculated against the entire graph.
		$prob = upper_hypercdf($interestingvertices,$totalvertices-$interestingvertices,$totalsupport,$hitsupport);
	}
	
	if($debug){print $motifstring."\t".$hitsupport."\t".$totalsupport."\t".$prob."\n";}
	
	$sigmotifs{$motifstring} = $prob;
	$freqmotifs{$motifstring} = $totalsupport;
	
	if($#motif >= $maxsize-1){
		return $biggerfim;
	}
	
	foreach my$sourceid (sort {$a cmp $b} keys %motiflabels){
	
		my@potentialedges;
		
		#Try connecting each node in motif to each other node
		foreach my$targetid (sort {$a cmp $b} keys %motiflabels){
			my$edge = $sourceid.$motiflabels{$sourceid}.'-'.$targetid.$motiflabels{$targetid};
			if($edge ~~ @motif){
				next;
			} else {
				push(@potentialedges,$edge);
			}
		}
		
		#Try adding a new edge with every possible label
		foreach my$newlabel (@possiblelabels){
			my$forwardedge = $sourceid.$motiflabels{$sourceid}.'-'.($maxid+1).$newlabel;
			push(@potentialedges,$forwardedge);
			my$backwardedge = ($maxid+1).$newlabel.'-'.$sourceid.$motiflabels{$sourceid};
			push(@potentialedges,$backwardedge);
		}
		
		TARGETLOOP: foreach my$edge (@potentialedges){
		
			my@newmotif = @motif;
			push(@newmotif,$edge);
			
			my$newmotifstring;
			my@optnewmotif;
			
			if(exists($motiftransformations{join(",",@newmotif)})){
				$newmotifstring = $motiftransformations{join(",",@newmotif)};
				@optnewmotif = split(",",$newmotifstring);
				
			} else {
			
				#Do motif optimization if never done before
				my$optnewmotifref = optimize_motif(\@newmotif);
				@optnewmotif = @{$optnewmotifref};
				
				if($debug){print "Transformed ".join(",",@newmotif)." into ".join(",",@optnewmotif)."\n"}
				$motiftransformations{join(",",@newmotif)} = join(",",@optnewmotif);
				$newmotifstring = join(",",@optnewmotif);
			
			}
			
			my$support = 0;
			
			if(defined($checkedmotifs{$newmotifstring})){
				$support = $checkedmotifs{$newmotifstring};
				
				#Already exists so check another
				next TARGETLOOP;
				
			} else {
				build_motif(\@optnewmotif,$threshold,$group,\%hitnodes);
				$biggerfim++;
			}
			
		
		}
	
	}
	
	
	return $biggerfim;
}

sub match_subgraph {

	#Check if a node set is a potential match for a given subgraph
	#Is called iteratively by always extending with new nodes until all nodes in the subgraph are covered

	my$motifref = shift @_;
	my$matchref = shift @_;
	
	my@motif = @{$motifref};
	my%match = %{$matchref};
	my@matchedids = values(%match);
	
	#Check how many edges each node should have to add as additional constraint
	my%edgecountbynode;
	my%edgecountbytarget;
	my%seen;
	my%fowloopedge;
	my%backloopedge;
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+).+-(\d+).+$/i){
			my$source = $1;
			my$target = $2;
			if((exists($seen{$source})) and (exists($seen{$target}))){
				if($source > $target){
					$fowloopedge{$source}{$target} = 1;
				} else {
					$backloopedge{$target}{$source} = 1;
				}
			}
			$seen{$source}=1;
			$seen{$target}=1;
			$edgecountbynode{$source}++;
			$edgecountbytarget{$target}++;
		} else {
			die "Cannot parse ".$edge."\n";
		}
	}
	
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+)(.+)-(\d+)(.+)$/i){
			my$sourceid = $1;
			my$sourcelabel = $2;
			my$targetid = $3;
			my$targetlabel = $4;
			
			#Because of the way that the motifs are constructed and sorted, either the source or target should have a match
			
			if(not(defined($match{$sourceid})) and (not(defined($match{$targetid})))){
				print "Undefined targetid and sourceid in motif ".$edge.". Continuing anyway\n";
				next; #While this shouldn't happen, the algorithm might still be able to salvage itself by skipping to the next edge
			}
			
			if(not(defined($match{$targetid}))){
				my$matchfound = 0;
				GRAPHTARGET: foreach my$graphtarget (keys %{$graph{$match{$sourceid}}}){
					
					#Labels need to match up
					unless(exists($label{$graphtarget}{$targetlabel})){next GRAPHTARGET;}
					
					#Can't already be used in another part of the motif
					if($graphtarget ~~ @matchedids){next GRAPHTARGET;}
					
					#Check for possible loop edges and check overlap if so
					if(exists($fowloopedge{$targetid})){
						foreach my$fowlooptarget (keys %{$fowloopedge{$targetid}}){
							if(exists($match{$fowlooptarget})){
								if(not(exists($graph{$graphtarget}{$match{$fowlooptarget}}))){
									next GRAPHTARGET;
								}
							}
						}
					}
					if(exists($backloopedge{$targetid})){
						foreach my$backlooptarget (keys %{$backloopedge{$targetid}}){
							if(exists($match{$backlooptarget})){
								if(not(exists($reversegraph{$graphtarget}{$match{$backlooptarget}}))){
									next GRAPHTARGET;
								}
							}
						}
					}
					
					#If it is used later as a source node it needs to have at least some outgoing edges
					if(defined($edgecountbynode{$targetid})){
						if(exists($graph{$graphtarget})){
							if(scalar(keys(%{$graph{$graphtarget}})) < $edgecountbynode{$targetid}) { 
								next GRAPHTARGET; 
							} 
						} else { next GRAPHTARGET; }
					}
					if(exists($reversegraph{$graphtarget})){
						if(scalar(keys(%{$reversegraph{$graphtarget}})) < $edgecountbytarget{$targetid}) { next GRAPHTARGET; }
					} else { next GRAPHTARGET; }
					
					my%trymatch = %match;
					$trymatch{$targetid} = $graphtarget;
					#if($debug){print "Trying ".$targetid." - ".$graphtarget."\n"}
					
					#Check if this target leads to a solution
					my$returnmatchref = match_subgraph(\@motif,\%trymatch);
					if($returnmatchref){
						$matchfound = 1;
						%match = %{$returnmatchref};
						#if($debug){print $targetid." - ".$graphtarget." OK \n"}
						last GRAPHTARGET;
					}
				}
				#If no matches are found, this match set is invalid and should return false
				unless($matchfound){
					return 0;
				}
			} elsif (not(defined($match{$sourceid}))){ 
				my$matchfound = 0;
				GRAPHTARGET: foreach my$graphtarget (keys %{$reversegraph{$match{$targetid}}}){
					
					#Labels need to match up
					unless(exists($label{$graphtarget}{$sourcelabel})){next GRAPHTARGET;}
					
					#Can't already be used in another part of the motif
					if($graphtarget ~~ @matchedids){next GRAPHTARGET;}
					
					#Check for possible loop edges and check overlap if so
					if(exists($fowloopedge{$sourceid})){
						foreach my$fowlooptarget (keys %{$fowloopedge{$sourceid}}){
							if(exists($match{$fowlooptarget})){
								if(not(exists($graph{$graphtarget}{$match{$fowlooptarget}}))){
									next GRAPHTARGET;
								}
							}
						}
					}
					if(exists($backloopedge{$sourceid})){
						foreach my$backlooptarget (keys %{$backloopedge{$sourceid}}){
							if(exists($match{$backlooptarget})){
								if(not(exists($reversegraph{$graphtarget}{$match{$backlooptarget}}))){
									next GRAPHTARGET;
								}
							}
						}
					}
					
					#If it is used as a source node it needs to have at least some outgoing edges
					if(exists($graph{$graphtarget})){
						if(scalar(keys(%{$graph{$graphtarget}})) < $edgecountbynode{$sourceid}) { next GRAPHTARGET; }
					} else { next GRAPHTARGET; }
					#If it is used later as a target node it needs to have at least some incoming edges
					if(defined($edgecountbytarget{$sourceid})){
						if(exists($reversegraph{$graphtarget})){
							if(scalar(keys(%{$reversegraph{$graphtarget}})) < $edgecountbytarget{$sourceid}) { next GRAPHTARGET; }
						} else { next GRAPHTARGET; }
					}
					
					my%trymatch = %match;
					$trymatch{$sourceid} = $graphtarget;
					#if($debug){print "Trying ".$sourceid." - ".$graphtarget."\n"}
					
					#Check if this target leads to a solution
					my$returnmatchref = match_subgraph(\@motif,\%trymatch);
					if($returnmatchref){
						$matchfound = 1;
						%match = %{$returnmatchref};
						#if($debug){print $sourceid." - ".$graphtarget." OK \n"}
						last GRAPHTARGET;
					}
				}
				#If no matches are found, this match set is invalid and should return false
				unless($matchfound){
					return 0;
				}
			}else {
				if(exists($graph{$match{$sourceid}})){
					unless(exists($graph{$match{$sourceid}}{$match{$targetid}})){
						return 0;
					}
				} else {
					return 0;
				}
			}
		} 
	}
	
	#Matches return only when there's a valid solution
	return \%match;
}

sub optimize_motif {
	
	#Change the numbering and the order of motifs a bit so that the most computational intensive steps are not at the very back
	#Added benefit is that some identical motifs will match eachother
	
	my$motifref = shift @_;
	
	my@motif = @{$motifref};
	
	my%transform;
	
	my%count; #count number of outgoing edges
	my%targets; #count number of incoming edges
	my%edgebynode; #create hash of edges by source node ids
	my%edgebytarget; #create hash of edges by target node ids
	my%motiflabel;
	my%lengthtoone; #save the distance to the root node
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+)(.+)-(\d+)(.+)$/i){
			my$sourceid = $1;
			$motiflabel{$sourceid} = $2;
			my$targetid = $3;
			$motiflabel{$targetid} = $4;
			$edgebynode{$sourceid}{$targetid} = $edge;
			$edgebytarget{$targetid}{$sourceid} = $edge;
			if($sourceid == 1){
				if(not($targetid == 1)){
					$lengthtoone{$targetid} = 1;
					$targets{$targetid}++;
				}
			} elsif ($targetid == 1) {
				$lengthtoone{$sourceid} = 0.9;
				$count{$sourceid}++;
			} else {
				$targets{$targetid}++;
				$count{$sourceid}++;
				if((exists($lengthtoone{$targetid})) and (exists($lengthtoone{$sourceid}))){
					if($lengthtoone{$targetid} > $lengthtoone{$sourceid} + 1){
						$lengthtoone{$targetid} = $lengthtoone{$sourceid} + 1;
					} 
					if ($lengthtoone{$sourceid} > $lengthtoone{$targetid} + 1){
						$lengthtoone{$sourceid} = $lengthtoone{$targetid} + 1;
					}
				} elsif (exists($lengthtoone{$sourceid})) {
					$lengthtoone{$targetid} = $lengthtoone{$sourceid} + 1;
				} elsif (exists($lengthtoone{$targetid})) {
					$lengthtoone{$sourceid} = $lengthtoone{$targetid} + 1;
				} else {
					#This should not happen as motifs are constructed away from the root node; but just in case
					$lengthtoone{$sourceid} =$maxsize;
					$lengthtoone{$targetid} =$maxsize;
				}
			}
		} else {
			die "Cannot parse ".$edge."\n";
		}
	}
	foreach my$node (keys %lengthtoone, 1){
		if(not(defined($count{$node}))){
			$count{$node} = 0;
		}
		if(not(defined($targets{$node}))){
			$targets{$node} = 0;
		}
	}
	
	#Sort source nodes based on the distance the the root node, number of outgoing edges and the label
	$transform{1} = 1;
	my$newid = 2;
	my@neworder = sort {$lengthtoone{$a} <=> $lengthtoone{$b} || $count{$b} <=> $count{$a} || $targets{$b} <=> $targets{$a} ||$labelhash{$motiflabel{$a}} <=> $labelhash{$motiflabel{$b}}} keys %lengthtoone;
	foreach my$oldid (@neworder){
		if($oldid == 1){
			die "Old id is 1 to be transformed into ".$newid."\n";
		}
		$transform{$oldid} = $newid;
		$newid++;
	}
	
	my@newmotif;
	if(exists($edgebynode{1}{1})){ #Give priority to root node self edges
		push(@newmotif, $edgebynode{1}{1});
	}

	#Sorted by proximity to the root node and number of edges
	foreach my$sourceid (@neworder){ #neworder contains ordering of all nodes except root node
		#only save edge if node is source node, or root node is source node
		if(exists($edgebynode{1}{$sourceid})){
			push(@newmotif,'1'.$motiflabel{'1'}.'-'.$transform{$sourceid}.$motiflabel{$sourceid});
		}
		if(exists($edgebynode{$sourceid}{1})){
			push(@newmotif,$transform{$sourceid}.$motiflabel{$sourceid}.'-'.'1'.$motiflabel{'1'});
		}
		if((not(exists($edgebynode{$sourceid}))) and (not(exists($edgebytarget{$sourceid})))){
			next;
		}
		foreach my$targetid (sort {$transform{$a} <=> $transform{$b}} keys %{$edgebynode{$sourceid}}){
			if($transform{$sourceid} <= $transform{$targetid}){
				push(@newmotif,$transform{$sourceid}.$motiflabel{$sourceid}.'-'.$transform{$targetid}.$motiflabel{$targetid});
			}
		}
		foreach my$targetid (sort {$transform{$a} <=> $transform{$b}} keys %{$edgebytarget{$sourceid}}){
			if($transform{$sourceid} < $transform{$targetid}){
				push(@newmotif,$transform{$targetid}.$motiflabel{$targetid}.'-'.$transform{$sourceid}.$motiflabel{$sourceid});
			}
		}
	}

	
	return \@newmotif;
}


#----------------------- Undirected graph -----------------------------

sub build_motif_und {
	
	my$motifref = shift @_;
	my$threshold = shift @_;
	my$group = shift @_;
	my$prevhits = shift @_;
	
	my@motif = @{$motifref};
	
	my%motiflabels;
	my$motifstring = join(",",@motif);
	my%nodesources;
	
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+)(.+)-(\d+)(.+)$/i){
			$motiflabels{$1} = $2;
			$motiflabels{$3} = $4;
			$nodesources{$3}++;
			$nodesources{$1}++;
		}
	}
	my$maxid = scalar keys %motiflabels;
	
	if($debug){print "Starting on ".$motifstring." \n";}

				
	my$hitsupport = 0; #Support with interesting nodes
	my%hitnodes;
	
	my$checkedgroupnodes = 0;
	
	foreach my$node (keys %{$group}){ #Check each interesting node if they are a root node
		
		#Check if part of the prevhit set
		unless(exists(${$prevhits}{$node})){next;}
		
		$checkedgroupnodes++;
		
		#Check if first node matches label (otherwise skip)
		unless(exists($label{$node}{$motiflabels{1}})){next;}
		
		my%supmatch = (1 => $node);
		my$supmatchref = match_subgraph_und(\@motif,\%supmatch);
		if($supmatchref){
				$hitsupport++;
				$hitnodes{$node} = 1;
		} 
	}
	
	$checkedmotifs{$motifstring} = $hitsupport;
	
	my$biggerfim = 0;
	
	unless($hitsupport >= $threshold){
		
		#Stop with this subgraph if it did not pass the required support
		return $biggerfim; 
		
	}
	
	$biggerfim++;
		
	my$totalsupport = 0; #Support with all nodes
	foreach my$node (keys %{$prevhits}){ #Go over all nodes
		
		#Check if already run with group nodes
		if(exists(${$group}{$node})){
			if(exists($hitnodes{$node})){
				$totalsupport++;
			}
			next;
		}
		
		#Check if first node matches label (otherwise skip) and has the required edges
		unless(exists($label{$node}{$motiflabels{1}})){next;}
		if(exists($nodesources{1})){
			my$c = 0;
			if(exists($graph{$node})){
				$c += scalar(keys(%{$graph{$node}}));
			}
			if(exists($reversegraph{$node})){
				$c +=scalar(keys(%{$reversegraph{$node}}));
			}
			if($c < $nodesources{1}){next;}
		}
		
		my%supmatch = (1 => $node);
		my$supmatchref = match_subgraph_und(\@motif,\%supmatch);
		if($supmatchref){
				$totalsupport++;
				$hitnodes{$node} = 1;
		}
	}
	
	#Calculate significance
	my$prob;
	if($nestedpval){
		#Nested P-value behaviour: Enrichment P-value is calculated against parent subgraph matches
		$prob = upper_hypercdf($checkedgroupnodes,(scalar keys %{$prevhits})-$checkedgroupnodes,$totalsupport,$hitsupport);
	} else {
		#Normal behaviour: Enrichment p-value is calculated against the entire graph.
		$prob = upper_hypercdf($interestingvertices,$totalvertices-$interestingvertices,$totalsupport,$hitsupport);
	}
	
	if($debug){print $motifstring."\t".$hitsupport."\t".$totalsupport."\t".$prob."\n";}
	
	$sigmotifs{$motifstring} = $prob;
	$freqmotifs{$motifstring} = $totalsupport;
	
	if($#motif >= $maxsize-1){
		return $biggerfim;
	}
	
	foreach my$sourceid (sort {$a cmp $b} keys %motiflabels){
	
		my@potentialedges;
		
		#Try connecting each node in motif to each other node
		foreach my$targetid (sort {$a cmp $b} keys %motiflabels){
			if($sourceid >= $targetid){next;}
			my$edgestr = $sourceid.$motiflabels{$sourceid}.'-'.$targetid.$motiflabels{$targetid};
			if($edgestr ~~ @motif){
				next;
			} else {
				push(@potentialedges,$edgestr);
			}
		}
		
		#Try adding a new edge with every possible label
		foreach my$newlabel (@possiblelabels){
			my$forwardedge = $sourceid.$motiflabels{$sourceid}.'-'.($maxid+1).$newlabel;
			push(@potentialedges,$forwardedge);
		}
		
		TARGETLOOP: foreach my$edge (@potentialedges){
		
			my@newmotif = @motif;
			push(@newmotif,$edge);
			
			my$newmotifstring;
			my@optnewmotif;
			
			if(exists($motiftransformations{join(",",@newmotif)})){
				$newmotifstring = $motiftransformations{join(",",@newmotif)};
				@optnewmotif = split(",",$newmotifstring);
				
			} else {
			
				#Do motif optimization if never done before
				my$optnewmotifref = optimize_motif_und(\@newmotif);
				@optnewmotif = @{$optnewmotifref};
				
				if($debug){print "Transformed ".join(",",@newmotif)." into ".join(",",@optnewmotif)."\n"}
				$motiftransformations{join(",",@newmotif)} = join(",",@optnewmotif);
				$newmotifstring = join(",",@optnewmotif);
			
			}
			
			my$support = 0;
			
			if(defined($checkedmotifs{$newmotifstring})){
				$support = $checkedmotifs{$newmotifstring};
				
				#Already exists so check another
				next TARGETLOOP;
				
			} else {
				build_motif_und(\@optnewmotif,$threshold,$group,\%hitnodes);
				$biggerfim++;
			}
			
		
		}
	
	}
	
	
	return $biggerfim;
}

sub match_subgraph_und {

	#Check if a node set is a potential match for a given subgraph
	#Is called iteratively by always extending with new nodes until all nodes in the subgraph are covered

	my$motifref = shift @_;
	my$matchref = shift @_;
	
	my@motif = @{$motifref};
	my%match = %{$matchref};
	my@matchedids = values(%match);
	
	#Check how many edges each node should have to add as additional constraint
	my%edgecountbynode;
	my%seen;
	my%fowloopedge;
	my%backloopedge;
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+).+-(\d+).+$/i){
			my$source = $1;
			my$target = $2;
			if((exists($seen{$source})) and (exists($seen{$target}))){
				if($source > $target){
					$fowloopedge{$source}{$target} = 1;
				} else {
					$backloopedge{$target}{$source} = 1;
				}
			}
			$seen{$source}=1;
			$seen{$target}=1;
			$edgecountbynode{$source}++;
			$edgecountbynode{$target}++;
		} else {
			die "Cannot parse ".$edge."\n";
		}
	}
	
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+)(.+)-(\d+)(.+)$/i){
			my$sourceid = $1;
			my$sourcelabel = $2;
			my$targetid = $3;
			my$targetlabel = $4;
			
			#Because of the way that the motifs are constructed and sorted, either the source or target should have a match
			
			if(not(defined($match{$sourceid})) and (not(defined($match{$targetid})))){
				print "Undefined targetid and sourceid in motif ".$edge.". Continuing anyway\n";
				next; #While this shouldn't happen, the algorithm might still be able to salvage itself by skipping to the next edge
			}
			
			if(not(defined($match{$targetid}))){
				my$matchfound = 0;
				GRAPHTARGET: foreach my$graphtarget (keys %{$graph{$match{$sourceid}}},keys %{$reversegraph{$match{$sourceid}}}){
					
					#Labels need to match up
					unless(exists($label{$graphtarget}{$targetlabel})){next GRAPHTARGET;}
					
					#Can't already be used in another part of the motif
					if($graphtarget ~~ @matchedids){next GRAPHTARGET;}
					
					#Check for possible loop edges and check overlap if so
					if(exists($fowloopedge{$targetid})){
						foreach my$fowlooptarget (keys %{$fowloopedge{$targetid}}){
							if(exists($match{$fowlooptarget})){
								if((not(exists($graph{$graphtarget}{$match{$fowlooptarget}}))) and (not(exists($reversegraph{$graphtarget}{$match{$fowlooptarget}})))){
									next GRAPHTARGET;
								}
							}
						}
					}
					if(exists($backloopedge{$targetid})){
						foreach my$backlooptarget (keys %{$backloopedge{$targetid}}){
							if(exists($match{$backlooptarget})){
								if((not(exists($reversegraph{$graphtarget}{$match{$backlooptarget}}))) and (not(exists($graph{$graphtarget}{$match{$backlooptarget}})))){
									next GRAPHTARGET;
								}
							}
						}
					}
					
					#If it is used later as a source node it needs to have at least some outgoing edges
					if(defined($edgecountbynode{$targetid})){
						my$c = 0;
						if(exists($graph{$graphtarget})){
							$c += scalar(keys(%{$graph{$graphtarget}}));
						}
						if(exists($reversegraph{$graphtarget})){
							$c += scalar(keys(%{$reversegraph{$graphtarget}}));
						} 
						if($c < $edgecountbynode{$targetid}){ next GRAPHTARGET; }
					}
					
					
					my%trymatch = %match;
					$trymatch{$targetid} = $graphtarget;
					#if($debug){print "Trying ".$targetid." - ".$graphtarget."\n"}
					
					#Check if this target leads to a solution
					my$returnmatchref = match_subgraph_und(\@motif,\%trymatch);
					if($returnmatchref){
						$matchfound = 1;
						%match = %{$returnmatchref};
						#if($debug){print $targetid." - ".$graphtarget." OK \n"}
						last GRAPHTARGET;
					}
				}
				#If no matches are found, this match set is invalid and should return false
				unless($matchfound){
					return 0;
				}
			}else {
				my$check=0;
				if(exists($graph{$match{$sourceid}})){
					if(exists($graph{$match{$sourceid}}{$match{$targetid}})){
						$check = 1;
					}
				} 
				if(exists($reversegraph{$match{$sourceid}})) {
					if(exists($reversegraph{$match{$sourceid}}{$match{$targetid}})){
						$check = 1;
					}
				}
				if($check == 0) {
					return 0;
				}
			}
		} 
	}
	
	#Matches return only when there's a valid solution
	return \%match;
}

sub optimize_motif_und {
	
	#Change the numbering and the order of motifs a bit so that the most computational intensive steps are not at the very back
	#Added benefit is that some identical motifs will match eachother
	
	my$motifref = shift @_;
	
	my@motif = @{$motifref};
	
	my%transform;
	
	my%count; #count number of outgoing edges
	my%targets; #count number of incoming edges
	my%edgebynode; #create hash of edges by source node ids
	my%edgebytarget; #create hash of edges by target node ids
	my%motiflabel;
	my%lengthtoone; #save the distance to the root node
	foreach my$edge (@motif){
		if($edge =~ m/^(\d+)(.+)-(\d+)(.+)$/i){
			my$sourceid = $1;
			$motiflabel{$sourceid} = $2;
			my$targetid = $3;
			$motiflabel{$targetid} = $4;
			$edgebynode{$sourceid}{$targetid} = $edge;
			$edgebytarget{$targetid}{$sourceid} = $edge;
			if($sourceid == 1){
				if(not($targetid == 1)){
					$lengthtoone{$targetid} = 1;
					$targets{$targetid}++;
				}
			} elsif ($targetid == 1) {
				$lengthtoone{$sourceid} = 1;
				$count{$sourceid}++;
			} else {
				$count{$targetid}++;
				$count{$sourceid}++;
				if((exists($lengthtoone{$targetid})) and (exists($lengthtoone{$sourceid}))){
					if($lengthtoone{$targetid} > $lengthtoone{$sourceid} + 1){
						$lengthtoone{$targetid} = $lengthtoone{$sourceid} + 1;
					} 
					if ($lengthtoone{$sourceid} > $lengthtoone{$targetid} + 1){
						$lengthtoone{$sourceid} = $lengthtoone{$targetid} + 1;
					}
				} elsif (exists($lengthtoone{$sourceid})) {
					$lengthtoone{$targetid} = $lengthtoone{$sourceid} + 1;
				} elsif (exists($lengthtoone{$targetid})) {
					$lengthtoone{$sourceid} = $lengthtoone{$targetid} + 1;
				} else {
					#This should not happen as motifs are constructed away from the root node; but just in case
					$lengthtoone{$sourceid} =$maxsize;
					$lengthtoone{$targetid} =$maxsize;
				}
			}
		} else {
			die "Cannot parse ".$edge."\n";
		}
	}
	foreach my$node (keys %lengthtoone, 1){
		if(not(defined($count{$node}))){
			$count{$node} = 0;
		}
		if(not(defined($targets{$node}))){
			$targets{$node} = 0;
		}
	}
	
	#Sort source nodes based on the distance the the root node, number of outgoing edges and the label
	$transform{1} = 1;
	my$newid = 2;
	my@neworder = sort {$lengthtoone{$a} <=> $lengthtoone{$b} || $count{$b} <=> $count{$a} || $labelhash{$motiflabel{$a}} <=> $labelhash{$motiflabel{$b}}} keys %lengthtoone;
	foreach my$oldid (@neworder){
		if($oldid == 1){
			die "Old id is 1 to be transformed into ".$newid."\n";
		}
		$transform{$oldid} = $newid;
		$newid++;
	}
	
	my@newmotif;
	if(exists($edgebynode{1}{1})){ #Give priority to root node self edges
		push(@newmotif, $edgebynode{1}{1});
	}

	#Sorted by proximity to the root node and number of edges
	foreach my$sourceid (@neworder){ #neworder contains ordering of all nodes except root node
		#only save edge if node is source node, or root node is source node
		if(exists($edgebynode{1}{$sourceid})){
			push(@newmotif,'1'.$motiflabel{'1'}.'-'.$transform{$sourceid}.$motiflabel{$sourceid});
		}
		if(exists($edgebynode{$sourceid}{1})){
			push(@newmotif,'1'.$motiflabel{'1'}.'-'.$transform{$sourceid}.$motiflabel{$sourceid});
		}
		if((not(exists($edgebynode{$sourceid}))) and (not(exists($edgebytarget{$sourceid})))){
			next;
		}
		foreach my$targetid (sort {$transform{$a} <=> $transform{$b}} keys %{$edgebynode{$sourceid}}){
			if($transform{$sourceid} <= $transform{$targetid}){
				push(@newmotif,$transform{$sourceid}.$motiflabel{$sourceid}.'-'.$transform{$targetid}.$motiflabel{$targetid});
			}
		}
		foreach my$targetid (sort {$transform{$a} <=> $transform{$b}} keys %{$edgebytarget{$sourceid}}){
			if($transform{$sourceid} < $transform{$targetid}){
				push(@newmotif,$transform{$sourceid}.$motiflabel{$sourceid}.'-'.$transform{$targetid}.$motiflabel{$targetid});
			}
		}
	}

	
	return \@newmotif;
}

#------------------------------------- Help and exit -----------------------

sub help_and_exit {
	print "SSM version 1.1\n";
	print "\n";
	print "Input parameters\n";
	print "-graph   \tTab delimited graph file with two columns.\n";
	print "-vertices\tText file with ids for vertices of interest\n";
	print "-labels  \tLabel file with two columns: Vertex id - Label\n";
	print "-output  \tOutputfile to print signficant motifs\n";
	print "-maxsize \tMaximum number of vertices allowed in the subgraph\n";
	print "-pvalue  \tMaximum pvalue allowed (default 0.05)\n";
	print "-bgfile  \tVariant to use a list of vertices as the graph background to compare against\n";
	print "-uniquelabel\tVariant where each node has exactly one label and this label must exactly match for the motif\n";
	print "-nestedpval\tVariant where the significance of the child motif is based on the parent matches\n";
	print "-undirected\tUndirected option where A->B = B->A and self-loops aren't allowed\n";
	print "\n";
	print "Example command:\n";
	print "perl subgraph.pl -graph example/example_graph.txt -labels example/example_labels.txt -vertices example/example_vertexset.txt -maxsize 2 -uniquelabel\n";
	exit;
}

#-----------------------------Hypergeometric calculation----------------

sub logfact {
   return gammln(shift(@_) + 1.0);
}

sub hypergeom {
   # There are m "bad" and n "good" balls in an urn.
   # Pick N of them. The probability of i or more successful selections:
   # (m!n!N!(m+n-N)!)/(i!(n-i)!(m+i-N)!(N-i)!(m+n)!)
   my ($n, $m, $N, $i) = @_;

   my $loghyp1 = logfact($m)+logfact($n)+logfact($N)+logfact($m+$n-$N);
   my $loghyp2 = logfact($i)+logfact($n-$i)+logfact($m+$i-$N)+logfact($N-$i)+logfact($m+$n);
   return exp($loghyp1 - $loghyp2);
}

sub gammln {
  my $xx = shift;
  my @cof = (76.18009172947146, -86.50532032941677,
             24.01409824083091, -1.231739572450155,
             0.12086509738661e-2, -0.5395239384953e-5);
  my $y = my $x = $xx;
  my $tmp = $x + 5.5;
  $tmp -= ($x + .5) * log($tmp);
  my $ser = 1.000000000190015;
  for my $j (0..5) {
     $ser += $cof[$j]/++$y;
  }
  -$tmp + log(2.5066282746310005*$ser/$x);
}

sub upper_hypercdf{ 
my ($n, $m, $N, $i) = @_;
my $hypercdf = 0;
for (my $iref=$i; $iref <= min($N,$n); $iref++)
#for (my $iref=0; $iref <= $i; $iref++)
{
  $hypercdf += hypergeom($n,$m,$N,$iref);
  #print $iref."\t".$hypercdf."\n";
}
return $hypercdf;
}

